navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || window.navigator.mozGetUserMedia;
window.URL = window.URL || window.webkitURL;

const ADDRESS = '<自分のIPアドレス>';
const PORT = '8889';
const web_socket = new WebSocket(`ws://${ADDRESS}:${PORT}`);
web_socket.binaryType = 'arraybuffer';
